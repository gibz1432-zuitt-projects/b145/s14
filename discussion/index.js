// console.log("Hello from JS");

/*this is a single-line comment
console.log("hello from Js");
console.log("hello from Js");
console.log("hello from Js");
*/
// console.log ("end of comment");
// console.log("example statement");

// let clientName = "Juan Dela Cruz";
// let contactNumber = "09951446335";

// let greetings;

// console.log(clientName);
// greetings = "hello Batch 145";
// console.log(greetings);
// let pangalan;
// pangalan = "John Doe"
// console.log(pangalan);

// let country = "Philppines";
// let province = 'Metro Manila';

// let headcount = 26;
// let grade = 98.7;
// let planetDistance =2e10;

// let isMarried = false;
// let inGoodConduct = true;

// let spouse = null;
// console.log(spouse);

// let grades = [98.7, 92.1, 90.2, 94.6]

// function createFullName (firstName, middleName, lastname){
// 	return firstName + ' ' + middleName + ' ' + lastname;
// }

// let fullName = createFullName('Juan','Dela','Cruz');
// console.log(fullName);

// let firstName = "John"
// let lastName = "Smith"
// let age = 30

// function createInfo (firstName,lastName,age) {
// return `${firstName} ${lastName} is ${age} years of age `
// }

// let info = createInfo('John', 'Smith', 30)
// console.log(info) 

// function createHobbies ([hobbies1,hobbies2,hobbies3]){
// 	return [hobbies1,hobbies2,hobbies3]
// }

// let hobbies = createHobbies(['Biking','Mountain Climbing', 'Swimming'])
// console.log(hobbies)

let bootcampSubjects = ["HTML", "CSS", "Bootstrap", "Javascript"];
console.log(bootcampSubjects);

let details = ["Keanu", "Reeves", 32, true];
console.log(details);

let cellphone = {
	brand: 'Samsung',
	model: 'A12',
	color: 'Black',
	serialNo: 'AX12002122',
	isHomecredit: true,
	features: ["Calling","Texting", "Ringing", "5G"],
	price: 8000
}

console.log(cellphone);

let personName = "Michael";
console.log(personName);

let pet = "dog";
console.log(`this is the initial value of var: ${pet}`);

pet = "cat";
console.log("This is the new value of the var: " + pet)

const PI = 3.14;
console.log(PI);

const year = 12;
console.log(year);

const familyName = 'Dela Cruz';
console.log(familyName);

familyName = 'Castillo';
console.log(familyName);
